package ru.korkmasov.tsc.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "Log out of the system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }
}

