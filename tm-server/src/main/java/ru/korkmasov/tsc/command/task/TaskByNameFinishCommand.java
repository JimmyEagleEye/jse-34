package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskByNameFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        serviceLocator.getTaskService().finishTaskByName(userId, TerminalUtil.nextLine());
    }

}
